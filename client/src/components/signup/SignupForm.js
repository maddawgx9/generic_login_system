import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import TextFieldGroup from '../common/TextFieldGroup';
import isEmpty from 'lodash/isEmpty';
import Validator from 'validator';

function validateInput(data) {
    let errors = {};

    if(Validator.isEmpty(data.username)) {
        errors.username = 'This field is required';
    }

    if(Validator.isEmpty(data.email)) {
        errors.email = 'This field is required';
    }

    if(!Validator.isEmail(data.email)) {
        errors.email = 'Email is invalid';
    }

    if(Validator.isEmpty(data.password)) {
        errors.password = 'This field is required';
    }

    if(Validator.isEmpty(data.passwordConfirmation)) {
        errors.passwordConfirmation = 'This field is required';
    }

    if(!Validator.equals(data.password, data.passwordConfirmation)) {
        errors.passwordConfirmation = 'Passwords must match!';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}

class SignupForm extends React.Component {
    constructor(props){
        super(props);
        this.state={
            username: '',
            email: '',
            password: '',
            passwordConfirmation: '',
            errors: {},
            isLoading: false,
            invalid: false
        };
        this.onChange=this.onChange.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.checkUserExists=this.checkUserExists.bind(this);
    }

    onChange(e){
        this.setState({ [e.target.name]: e.target.value });
    }

    isValid() {
        const { errors, isValid } = validateInput(this.state);

        if(!isValid) {
            this.setState({errors});
        }

        return isValid;
    }

    onSubmit(e){
        e.preventDefault();

        if(this.isValid())
        {
            this.setState({ errors: {}, isLoading: true });
            this.props.userSignupRequest(this.state).then(
                (res) => {
                    this.props.addFlashMessage({
                        type: 'success',
                        text: 'You signed up successfully. Welcome!'
                    });
                    this.context.router.history.push('/');
                },
                (err) => this.setState({ errors: err.response.data, isLoading: false })
            );
        }
    }

    checkUserExists (e) {
        console.log('checking...');
        const field = e.target.name;
        const val = e.target.value;
        if(val!=='') {
            this.props.isUserExists(val).then( res => {
                let errors  = this.state.errors;
                let invalid;
                console.log('checking2: '+res.data.user);
                if (res.data.user && res.data.user != '') {
                    console.log('USER FOUND!');
                    errors[field] = 'There is a user with this '+field+' already.';
                    invalid=true;
                } else {
                    console.log('NO USER');
                    delete errors[field];
                    invalid=false;
                }
                this.setState({ errors, invalid: !isEmpty(errors)});
            });
        }
    }

    render() {
        const { errors } = this.state;

        return (
            <form onSubmit={this.onSubmit}>
                <h1>Join our community!</h1>

                <TextFieldGroup
                    error={errors.username}
                    label="Username"
                    onChange={this.onChange}
                    checkUserExists={this.checkUserExists}
                    value={this.state.username}
                    field="username"
                />

                <TextFieldGroup
                    error={errors.email}
                    label="E-mail"
                    onChange={this.onChange}
                    checkUserExists={this.checkUserExists}
                    value={this.state.email}
                    field="email"
                />

                <TextFieldGroup
                    error={errors.password}
                    label="Password"
                    onChange={this.onChange}
                    value={this.state.password}
                    field="password"
                />

                <TextFieldGroup
                    error={errors.passwordConfirmation}
                    label="Password Confirmation"
                    onChange={this.onChange}
                    value={this.state.passwordConfirmation}
                    field="passwordConfirmation"
                />

                <div className="form-group">
                    <button disabled={this.state.isLoading || this.state.invalid} className="btn btn-primary btn-lg">
                        Sign up
                    </button>
                </div>
            </form>
        );
    }
}

SignupForm.propTypes = {
    userSignupRequest: PropTypes.func.isRequired,
    addFlashMessage: PropTypes.func.isRequired,
    isUserExists: PropTypes.func.isRequired
}
SignupForm.contextTypes = {
    router: PropTypes.object.isRequired
}
export default SignupForm;
