import express from 'express';
import commonValidations from '../shared/validations/signup';
import bcrypt from 'bcrypt';
import isEmpty from 'lodash/isEmpty';
import User from '../models/user';
import pkg from '../../package.json';

let router = express.Router();

function validateInput(data, otherValidations) {
    let { errors } = otherValidations(data);

    return User.findOne({
        $or: [
            {email: data.email},
            {username: data.username}
            ]
        }).exec().then( user => {
            if (user) {
                console.log('FOUND USER: '+user.username+" == "+data.username);
                if (user.username == data.username) {
                    errors.username = 'There is a user with this username already.';
                }
                if (user.email == data.email) {
                    errors.email = 'There is a user with this email already.';
                }
            }
            else {
                console.log('FAILED TO FIND USER');
            }
            return {
                errors,
                isValid: isEmpty(errors)
            };
        });
}

router.get('/:identifier',(req, res) => {
    User.find({
        $or: [
            {email: req.params.identifier},
            {username: req.params.identifier}
            ]
        }, 'username email').exec().then( user => {
            res.json({ user });
        });
    });

router.post('/', (req, res) => {
    validateInput(req.body, commonValidations).then(({ errors, isValid }) => {
        if (isValid) {
            const { username, password, email } = req.body;
            const passwordDigest = bcrypt.hashSync(password,10);

            var newUser = new User({ username, email, passwordDigest });
            newUser.save().then(() => {
                    console.log('success!');
                    res.json({success: true});
                },(err) => {
                    console.log('failed: '+err);
                    res.status(500).json({error: err});
                });
        } else {
            res.status(400).json(errors);
        }
    });
});

export default router;
