import express from 'express';
import User from '../models/user';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../config.js';

let router = express.Router();

router.post('/', (req, res) => {
    const { identifier, password } = req.body;

    User.findOne({
        $or: [
            { username: identifier },
            { email: identifier }
        ]}).exec().then( user => {
            console.log("RETURNED!");
            if(user) {
                if (bcrypt.compareSync(password, user.passwordDigest)) {
                    const token = jwt.sign({
                        id: user.id,
                        username: user.username
                    }, config.jwtSecret);
                    res.json({ token });
                } else {
                    res.status(401).json({ errors: { form: 'Invalid Credentials' } });
                }
            } else {
                res.status(401).json({ errors: { form: 'Invalid Credentials' } });
            }
        });
});

export default router;
