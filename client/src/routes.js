import React from 'react'
import { Route } from 'react-router'

import App from './components/App'
import Greetings from './components/Greetings'

export default (
    <div>
        <Route path="/" component={App} />
    </div>
);
