import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import pkg from '../package.json';

import users from './routes/users';
import auth from './routes/auth';

let app = express();

app.use(bodyParser.json());

app.use('/api/users', users);
app.use('/api/auth', auth);

mongoose.connect(pkg.db.url);

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error.'));

app.listen(3002, () => console.log('Running on 3002'));
