import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const userSchema = mongoose.Schema({
    username: { type: String, unique: '\"{VALUE}\" is already in use!' },
    email: { type: String, unique: '\"{VALUE}\" is already in use!' },
    passwordDigest: String,
});

userSchema.plugin(uniqueValidator);

var User = mongoose.model('User',userSchema);

export default User;
