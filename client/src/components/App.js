import React from 'react'
import NavigationBar from './NavigationBar'
import { Route } from 'react-router'

import Greetings from './Greetings'
import SignupPage from './signup/SignupPage'
import FlashMessagesList from './flash/FlashMessagesList';
import LoginPage from './login/LoginPage'

class App extends React.Component {
    render(){
        return (
            <div className="container">
                <NavigationBar />
                <FlashMessagesList />
                <Route exact path="/" component={Greetings}/>
                <Route path="/signup" component={SignupPage}/>
                <Route path="/login" component={LoginPage}/>
            </div>
        );
    }
}

export default App;
